const net = require('net');
const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();

const PORT_TCP = process.env.TCP_PORT || 2006;
const PORT_HTTP = process.env.HTTP_PORT || 2007;
const MONGODB_URI = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;

mongoose.connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

db.on('error', (error) => {
  console.error(`MongoDB connection error: ${error}`);
});

db.once('open', () => {
  console.log('MongoDB connected successfully');

  const DataSchema = new mongoose.Schema({
    imei: String,
    ipAddress: String,
    data: String,
    dateTime: Date,
    serverhittime: Date,
    type: String
  }, { collection: 'ipv6' });

  const DataModel = mongoose.model('Data', DataSchema);

  // TCP Server
  const server = net.createServer({ family: 'IPv6' }, (socket) => {
    const clientAddress = socket.remoteAddress;

    console.log(`Client connected from IP: ${clientAddress}`);

    socket.on('data', async (data) => {
      let jsonData;
      let stringData;

      try {
        jsonData = JSON.parse(data.toString());
        stringData = JSON.stringify(jsonData[0]);
      } catch (error) {
        stringData = data.toString();
        console.error(`Error parsing JSON data: ${error}`);
      }
      const imei=null;
      const ipAddress = null;
try{
       imei = jsonData ? jsonData[0].t : null;
      dateTime = jsonData ? new Date(jsonData[0].T) : null;
    }catch{
      imei=null;
      dateTime=null;
    }
     const serverHitTime = new Date();

      await saveToMongoDB(imei,"TCP", ipAddress, stringData, dateTime, serverHitTime);
      const emptyString = '';
      const buffer = Buffer.from(emptyString, 'utf8');
      socket.write(buffer);
      console.log(`Received data: ${stringData}`);
    });

    socket.on('end', () => {
      console.log('Client disconnected');
    });

    socket.on('error', (err) => {
      console.error(`Socket error: ${err}`);
    });
  });

  server.listen(PORT_TCP, '::', () => {
    console.log(`Server listening on port ${PORT_TCP} with IPv6`);
  });

  server.on('error', (err) => {
    console.error(`Server error: ${err}`);
  });

  // HTTP Server
  const app = express();
  app.use(bodyParser.urlencoded({ extended: true }));

  app.post('/parsedata', async (req, res) => {
    const vltjson = req.body.vltjson;
    let jsonData;
    let stringData;

    try {
      jsonData = JSON.parse(vltjson);
      stringData = JSON.stringify(jsonData[0]);
    } catch (error) {
      stringData = vltjson;
      console.error(`Error parsing JSON data: ${error}`);
    }
    const imei=null;
    const ipAddress = null;
try{
     imei = jsonData ? jsonData[0].t : null;
    dateTime = jsonData ? new Date(jsonData[0].T) : null;
  }catch{
    imei=null;
    dateTime=null;
  }
  
  
    const serverHitTime = new Date();

    await saveToMongoDB(imei,"HTTP", ipAddress, stringData, dateTime, serverHitTime);

    console.log(`Received data: ${stringData}`);
    res.send('Data received and saved to MongoDB');
  });

  app.listen(PORT_HTTP, () => {
    console.log(`HTTP server listening on port ${PORT_HTTP}`);
  });
  async function saveToMongoDB(imei,type, ipAddress, data, dateTime, serverHitTime) {
    try {

      console.log(type);
      const newData = new DataModel({ imei,type, ipAddress, data, dateTime, serverhittime: serverHitTime });
      await newData.save();

      console.log('Data saved to MongoDB using Mongoose');
    } catch (error) {
      console.error(`Error saving data to MongoDB using Mongoose: ${error}`);
    }
  }
});
