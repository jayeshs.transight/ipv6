const net = require('net');
const mongoose = require('mongoose');

const PORT = 2006;
const MONGODB_URI = 'mongodb://localhost:27017/ipv6';

mongoose.connect(MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

db.on('error', (error) => {
  console.error(`MongoDB connection error: ${error}`);
});

db.once('open', () => {
  console.log('MongoDB connected successfully');

  const DataSchema = new mongoose.Schema({
    imei: String,
    ipAddress: String,
    data: String,
    dateTime: Date, // Add a field for date and time
  }, { collection: 'ipv6' });

  const DataModel = mongoose.model('Data', DataSchema);

  const server = net.createServer({ family: 'IPv6' }, (socket) => {
    const clientAddress = socket.remoteAddress; // Get the IP address of the client

    console.log(`Client connected from IP: ${clientAddress}`);

    socket.on('data', async (data) => {
      let jsonData;
      let stringData;

      try {
        jsonData = JSON.parse(data.toString());
        stringData = JSON.stringify(jsonData[0]); // Convert the data object to a string
      } catch (error) {
        // If parsing fails, store the raw data as a string
        stringData = data.toString();
        console.error(`Error parsing JSON data: ${error}`);
      }

      // Extract specific fields
      const imei = jsonData ? jsonData[0].t : null;
      const ipAddress = clientAddress;
      const dateTime = jsonData ? new Date(jsonData[0].T) : null; // Extract date and time from the "T" field

      // Save the extracted data to MongoDB using Mongoose
      await saveToMongoDB(imei, ipAddress, stringData, dateTime);

      console.log(`Received data: ${stringData}`);
    });

    socket.on('end', () => {
      console.log('Client disconnected');
    });

    socket.on('error', (err) => {
      console.error(`Socket error: ${err}`);
    });
  });

  server.listen(PORT, '::', () => {
    console.log(`Server listening on port ${PORT} with IPv6`);
  });

  server.on('error', (err) => {
    console.error(`Server error: ${err}`);
  });

  async function saveToMongoDB(imei, ipAddress, data, dateTime) {
    try {
      const newData = new DataModel({ imei, ipAddress, data, dateTime });
      await newData.save();

      console.log('Data saved to MongoDB using Mongoose');
    } catch (error) {
      console.error(`Error saving data to MongoDB using Mongoose: ${error}`);
    }
  }
});
