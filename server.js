const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();
const PORT = process.env.SERVER_PORT || 3000;

// MongoDB connection
const MONGODB_URI = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
mongoose.connect(MONGODB_URI, {
  //useNewUrlParser: true,
  //useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on('error', (error) => {
  console.error(`MongoDB connection error: ${error}`);
});

db.once('open', () => {
  console.log('MongoDB connected successfully');

  const DataSchema = new mongoose.Schema({
    imei: String,
    ipAddress: String,
    data: String,
    dateTime: Date,
    serverhittime: Date,
    type: String
  }, { collection: 'ipv6' });
  const DataModel = mongoose.model('Data', DataSchema);

  // API endpoint to fetch last 100 data entries or filter by date range
  app.get('/api/data', async (req, res) => {
    try {
      const { startDate, endDate } = req.query;

      let query = DataModel.find().sort({ serverhittime: 1 });

      if (startDate && endDate) {
        const start = new Date(startDate);
        
        // Set end time to 23:59:59
        const end = new Date(endDate);
        end.setHours(23);
        end.setMinutes(59);
        end.setSeconds(59);
      
        query = DataModel.find({
          serverhittime: {
            $gte: start,
            $lte: end
          }
        }).sort({ serverhittime: 1 });
      }

      const data = await query.exec();
      res.json(data.reverse()); // Reverse the order to get the latest entries first
    } catch (error) {
      console.error(`Error fetching data: ${error}`);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });

  app.use(express.static('public'));

  app.listen(PORT, () => {
    console.log(`Server running on http://localhost:${PORT}`);
  });
});
